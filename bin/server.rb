#!/usr/bin/env ruby1.9.1


$: << File.join( File.dirname( __FILE__ ), '../lib' )

require 'optparse'
require 'webrick'

require 'dochandler'
require 'servlet'


# the directory containing the data to be used during operation
DATA_DIR = File.realpath( File.join( File.dirname( __FILE__ ), '../data' ) )
# the default document file name to use
DEF_FNAME = 'document.html'


# the default config options
args = {
	BindAddress: '0.0.0.0',
	Port: 8808,
	filename: File.join( DATA_DIR, DEF_FNAME )
}

# parse the command-line arguments (if any)
OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [options]"
  opts.on( '-s', '--server HOST', 'the hostname or IP to bind the server to' ) { |val| args[:BindAddress] = val }
  opts.on( '-p', '--port PORT', 'the port to bind the server to' ) { |val| args[:Port] = val.to_i }
  opts.on( '-f', '--file PATH', 'the path to the file where to store the document' ) { |val| args[:filename] = val }
  opts.on_tail( '-h', '--help', 'shows this help and exit' ) do |val|
    puts opts
    exit 0
  end
end.parse!


# initialise the document handler
PlaneCrash::DocumentHandler.get_instance( args.delete( :filename ) )

# initialise the server
server = WEBrick::HTTPServer.new args
# mount the API end-point
server.mount '/api/v1/plane', PlaneCrash::API

# trap INT so the server may shutdown properly
trap( 'INT' ) { server.shutdown }

# finally, start the server
server.start

# this point is not reached until the server stops
exit 0

