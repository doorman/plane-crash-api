Welcome to the sample Latest_Plane_Crash API repository


REPOSITORY OUTLINE
==================

|- bin   - contains the server starter script
|- data  - contains the document storage data
|- lib   - all of the library files


RUNNING THE CODE
=================

To run the code, you need to have Ruby 1.9.3 on your machine. No additional gems
are needed. To start the server, simply type:

  bin/server.rb

This will start a server listening on 0.0.0.0:8808 . If you want, you can give
it some additional arguments. Run bin/server.rb --help for info on these.

Once the server is up and running, you can then curl to get or post a document.
Example for getting the current version of the document:

	curl http://localhost:8808/api/v1/plane

To store a new version of the document, use:

  curl -XPOST --data @path_to_new_version.html -H 'Content-Type: text/html'
    http://localhost:8808/api/v1/plane

