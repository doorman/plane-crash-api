require_relative 'dochandler'


module PlaneCrash
	
	
	class API < ::WEBrick::HTTPServlet::AbstractServlet
		
		
		# responds to GET requests
		def do_GET request, response
			
			response.status = 200
			response['Content-Type'] = 'text/html'
			response.body = DocumentHandler.get_instance.get_file
			
		end
		
		
		# responds to POST requests
		def do_POST request, response
			
			DocumentHandler.get_instance.post_file( request.body )
			response.status = 200
			
		end
		
		
	end
	
	
end

