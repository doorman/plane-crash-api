require 'thread'


module PlaneCrash
	
	
	class DocumentHandler
		
		
		# the number of concurrent threads to use
		NO_THREADS = 24
		
		# the number to compute the fibonacci sum for
		FIB_NUMBER = 34
		
		
		# the document handler instance to be used during execution
		@@instance = nil
		
		
		# get or creates a new DocumentHandler instance
		def self.get_instance filepath = nil
			
			# if an object exists already, return it
			return @@instance if @@instance
			
			# can we create a new one ?
			raise ArgumentError, "No file name specified!" unless filepath
			
			# create an instance
			@@instance = self.new filepath

			return @@instance
			
		end
		
		
		# initialises a new document handler document
		# @param filename [String] the path to the file where to store the document
		def initialize filename
			
			# initialise the worker threads
			@workers = NO_THREADS.times.map do
				Thread.new do
					Thread.current[:input] = Queue.new
					self.worker_loop
				end
			end
			
			@fname = filename
			# try to retrieve the file if it exists
			@doc = File.read( @fname ) rescue ""
			
			# the queue used for storing the new versions of the document
			@store_queue = Queue.new
			# start the storage thread
			Thread.new { self.store_loop }
			
		end
		
		
		# gets the current version of the file by enqueuing a new request
		# and getting back the result
		def get_file
			
			# find the best worker thread to enqueue the request on
			best = @workers[0][:input]
			@workers.each do |thr|
				no_items = thr[:input].size
				if no_items == 0
					# this is the best since it's empty
					best = thr[:input]
					break
				elsif no_items < best.size
					best = thr[:input]
				end
			end
			
			# create a new temporary queue to receive the response
			q = Queue.new
			# enqueue the request
			best.push q
			# wait for the result
			current_doc = q.pop
			
			return current_doc
			
		end
		
		
		# enqeueues a new version of the document for later storing
		def post_file contents
			
			# just enqueue the new version
			@store_queue.push contents.dup
			
			return true
			
		end
		
		
		### ### ### ###
		protected
		### ### ### ###
		
		
		# waits for the next computing job and executes it
		def worker_loop
			
			# get the input queue to get jobs from
			queue = Thread.current[:input]
			
			while out_queue = queue.pop do
				# simulate work being done
				self.fib FIB_NUMBER
				# return the current version of the document
				curr_v = @doc.dup
				out_queue.push curr_v
			end
			
		end
		
		
		# serialises the document put requests by handling them one by one
		def store_loop
			
			while new_version = @store_queue.pop do
				# save it to disk first
				begin
					File.open( @fname, 'w' ) { |io| io.write new_version }
				rescue
					# ignore, for now
				end
				# update the current version now that the file has been written
				@doc = new_version.dup
				new_version = nil
			end
			
		end
		
		
		# computes the fibonacci sum for n recursively
		# @param n [Integer] the number to compute the sum for
		# @return [Integer] the computed sum
		def fib n
			
			return 0 if n <= 0
			return 1 if n == 1
			return self.fib( n - 1 ) + self.fib( n - 2 )
			
		end
	
	
	end
	
	
end

